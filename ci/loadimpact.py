#!/usr/bin/python
"""
This is a script used to communicate with Load Impact to start and get result of a specific Test.
It could be useful in a Continious Integration environment, for instance.

Usage: ./loadinpacy.py -c <config_id> -k <keyfile> -t [timeout] -s [sleep] [-v]

@requires python-requests http://docs.python-requests.org/
@see http://support.loadimpact.com/knowledgebase/articles/265475-using-the-load-impact-api-to-run-tests-programmati
@author Johan Wettergren
"""

import sys, os, time
import re
import requests
from requests import RequestException
from requests import HTTPError
from optparse import OptionParser

def rest_get(url):
	if options.verbose:
		print "[DEBUG] GET %s" % url
	response = requests.get(url, auth=(api_token, ''), timeout=options.timeout)
	if response.status_code != 200:
		response.raise_for_status()
	return response.json()

def rest_post(url):
	if options.verbose:
		print "[DEBUG] POST %s" % url
	response = requests.post(url, auth=(api_token, ''), timeout=options.timeout)
	if response.status_code != 200:
		response.raise_for_status()
	return response.json()

# Define the arguments...
parser = OptionParser(description="LoadImpact API Tool")
parser.add_option("-c", "--config", action="store", type="int", dest="config_id", default=None, help="ID of Test Config to run")
parser.add_option("-k", "--key", action="store", type="string", dest="keyfile", default=None, help="Path to the API keyfile")
parser.add_option("-t", "--timeout", action="store", type="int", dest="timeout", default=300, help="Timeout for the test run, default 300s")
parser.add_option("-s", "--sleep", action="store", type="int", dest="sleep", default=30, help="Sleep this between is-done polls, defaults 30s")
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Print verbobe and debug data")

# ...and parse them
(options, args) = parser.parse_args()

# Valdiate the mandatory options
if options.config_id is None:
	print "[ERROR] No config argument provided"
	print "[ERROR] Usage: ./loadimpact.py -c <config_id> -k <keyfile>"
	sys.exit(6)

if options.keyfile is None:
	print "[ERROR} No key file argument provided"
	print "[ERROR] Usage ./loadimpact.py -c <config_id> -k <keyfile>"
	sys.exit(6)

if options.sleep < 1:
	print "[ERROR] Sleep delay must be a positive integer"
	sys.exit(6) 

if options.verbose:
	print "[DEBUG] ConfigID: %s" % options.config_id
	print "[DEBUG] API Key File: %s" % options.keyfile
	print "[DEBUG] Poll Sleep Delay: %s" % options.sleep

# This is the API Key of your Load Impact account.
# Generate it on your Account Page when logged in.
try:
	with open(options.keyfile) as f_key:
		api_token = f_key.read().strip()
except IOError as keyerr:
	print "[ERROR] Cannot read the key file '%s'" % options.keyfile
	sys.exit(7)

# Validate the key content, so that no odd characters has found
# its way into the file
if len(re.findall('^[a-f0-9]{64}$', api_token)) != 1:
	print "[ERROR] API Key appears to be on invalid format"
	sys.exit(8)

# Just a short-hand for the config parameter
config_id = options.config_id

# Some API statics
url_base = "https://api.loadimpact.com/v2"

# Let's get started!
try:

	# Start the test with a HTTP POST
	start_url = "%s/test-configs/%s/start" % (url_base, config_id)
	print "[INFO] Starting Test"

	# It should return a "test" object
	test = rest_post(start_url)
	print "[INFO] Response object: %s" % str(test)

	# Now the test is running, poll until it's reported done
	done = False
	while(done == False):

		# Don't hammer it, sleep a bunch of seconds  
		time.sleep(options.sleep)
  
		# Get the current execution status
		poll_url = "%s/tests/%s" % (url_base, test["id"])

		status = rest_get(poll_url)
		print "[INFO] Current status: %s" % status["status_text"]

		# Test if it's done
		if(status["status"] > 2):
			done = True

	# Now it's done, get the final results
	final_url = "%s/tests/%s/results" % (url_base, test["id"])
	results = rest_get(final_url)

	# Print the user load time of the test?
	#last = len(results.__li_user_load_time); 
	#print results.__li_user_load_time[$last]->value; 
	print "[INFO] Test completed"
	print "[INFO] Result object: %s" % str(results)
	sys.exit(0)

except RequestException as req_ex:
	print "[ERROR] Caught Request Exception: %s" % str(req_ex)
	sys.exit(3)

except HTTPError as http_ex:
	print "[ERROR] Caught HTTP Exception: %s" % str(http_ex)
	sys.exit(4)

except Exception as ex:
	print "[ERROR] Caught General Exception: %s" % str(ex)
	sys.exit(5)

